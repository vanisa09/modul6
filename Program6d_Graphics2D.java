import javax.swing.*;
import java.awt.*;
import java.awt.geom.Arc2D;

public class Program6d_Graphics2D extends JPanel {
    public static void main(String[] a) {
        JFrame f = new JFrame();
        f.setTitle("Arc2D");
        f.setSize(500, 300);
        f.add(new Program6d_Graphics2D());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Stroke wideStroke = new BasicStroke(12.0f);
        double x = 10.5;
        double y = 10.5;
        int rectHeight = 100;
        int rectWidth  = 200;
        int w = getSize().width;
        int h = getSize().height;
        g2.setStroke(wideStroke);
        g2.draw( new Arc2D.Double(x,  y, rectWidth , rectHeight,90,135,Arc2D.OPEN));
    }
}
