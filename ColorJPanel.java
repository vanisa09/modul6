import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class ColorJPanel extends JPanel {
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(new Color(255,0,0));
		g.fillRect(15, 25, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 40);
		g.setColor(new Color(0.50f , 0.75f, 0.0f));
		g.fillRect(15, 50, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 65);
		g.setColor(Color.BLUE);
		g.fillRect(15, 75, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 90);
		g.setColor(Color.BLACK);
		g.fillRect(15, 125, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 115);
		g.setColor(Color.YELLOW);
		g.fillRect(15, 150, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 130);
		g.setColor(Color.PINK);
		g.fillRect(15, 175, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 145);
		g.setColor(Color.ORANGE);
		g.fillRect(15, 200, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 160);
		g.setColor(Color.CYAN);
		g.fillRect(15, 225, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 190);
		g.setColor(Color.GRAY);
		g.fillRect(15, 250, 100, 20);
		g.drawString("Current RGB:"+ g.getColor(), 130 , 205);
		
		Color color = Color.MAGENTA;
		g.setColor(color);
		g.fillRect(15, 100, 100, 20);
		g.drawString("RGB values: ", 130,115);
	}
}
