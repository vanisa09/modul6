import javax.swing.*;
import java.awt.*;
import java.awt.Stroke;
import java.awt.geom.RoundRectangle2D;

public class Program6c_Graphics2D extends JPanel {
    public static void main(String[] a) {
        JFrame f = new JFrame();
        f.setTitle("RoundRectangle2D");
        f.setSize(500, 300);
        f.add(new Program6c_Graphics2D());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        float[] dash1 = { 2f, 0f, 2f };
        double x = 10.5;
        double y = 10.5;
        int rectHeight = 100;
        int rectWidth  = 200;
        Stroke dashed = new BasicStroke(1,BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_ROUND,1.0f,dash1,2f);
        g2.setStroke(dashed);
        g2.draw( new RoundRectangle2D.Double(x,  y, rectWidth , rectHeight,20,20));
    }
}
