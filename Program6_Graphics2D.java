import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
public class Program6_Graphics2D extends JPanel {
    public static void main(String[] a) {
        JFrame f = new JFrame();
        f.setTitle("Line2D");
        f.setSize(500, 300);
        f.add(new Program6_Graphics2D());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        double x = 10.5;
        double y = 10.5;
        int rectHeight = 100;
        int rectWidth  = 250;
        g2.draw( new Line2D.Double(x,  y + rectHeight - 1,  x + rectWidth , y));
    }
}
