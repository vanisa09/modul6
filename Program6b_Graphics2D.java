import javax.swing.*;
import java.awt.*;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

public class Program6b_Graphics2D extends JPanel {
    public static void main(String[] a) {
        JFrame f = new JFrame();
        f.setTitle("Rectangle2D");
        f.setSize(500, 300);
        f.add(new Program6b_Graphics2D());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        double x = 10.5;
        double y = 10.5;
        int rectHeight = 100;
        int rectWidth  = 200;
        Stroke stroke = new BasicStroke();
        g2.setStroke(stroke);
        g2.draw( new Rectangle2D.Double(x,  y, rectWidth , rectHeight));
    }
}
